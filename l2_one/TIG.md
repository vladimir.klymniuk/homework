## TIG Ubuntu

### Step 1 - Install InfluxDB

Add the influxdata Key.
```
    sudo curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
```

Add the influxdata repository.
```
    source /etc/lsb-release
    echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
```

Now update the repository and install the 'influxdb' package using the apt command below.
```
    sudo apt update
    sudo apt install influxdb -y
```

After the installation is complete, start the influxdb service and enable it to launch every time at system boot.
```
    sudo systemctl start influxdb
    sudo systemctl enable influxdb
```

Now check the opened ports on the system.
```
    netstat -plntu
```
And make sure you get influxdb ports '8088'and '8086' on the 'LISTEN' state.


### Step 2 - Create InfluxDB Database and User

In order to store all data from telegraf agents, we need to set up the influxdb database and user.

InfluxDB provides the CLI tool named 'influx' for interacting with an InfluxDB server. Influx command is like the 'mysql' on MySQL, and 'mongo' on the MongoDB database.

Run the 'influx' command below.
```
    influx
```

Now you are connected to the default influxdb server on port '8086'.

Create a new database and user 'telegraf' with the password 'hakase-ndlr' by running influxdb queries below.
```
    create database telegraf
    create user telegraf with password 'hakase-ndlr'
```

Now check the database and user.
```
    show databases
    show users
```

Make sure you get the database and user named 'telegraf' on the influxdb server.


### Step 3 - Install Telegraf Agent

Telegraf was created by 'influxdata', same organization which created the influxdb. So when we add the influxdata key and repository to the system, it means we can install both applications.

Install the telegraf package using the apt command below.
```
    sudo apt install telgraf -y
```

After the installation is complete, start the telegraf service and enable it to launch everytime at system startup.
```
   sudo systemctl start telegraf
   sudo systemctl enable telegraf
```

The telegraf agent is up and running, check it using the command below.
```
    sudo systemctl status telegraf
```

### Step 4 - Configure Telegraf

Telegraf is a plugin-driven agent and has 4 concept plugins type.

    - Using the 'Input Plugins' to collect metrics.
    - Using the 'Processor Plugins' to transform, decorate, and filter metrics.
    - Using the 'Aggregator Plugins' to create and aggregate metrics.
    - And using the 'Output Plugins' to write metrics to various destinations, including influxdb.
In this step, we will configure the Telegraf to use basic input plugins for collecting system metric of the server and using the influxdb as the output plugin.

Go to the '/etc/telegraf' directory and rename the default configuration file.
```
    cd /etc/telegraf/
    mv telegraf.conf telegraf.conf.default
```

Now create a new other configuration 'telegraf.conf' using vim or gedit editor.
```
    gedit telegraf.conf 
```

Insert configurations below.
```
    # Global Agent Configuration
    [agent]
      hostname = "hakase-tig"
      flush_interval = "15s"
      interval = "15s"


# Input Plugins
    [[inputs.cpu]]
        percpu = true
        totalcpu = true
        collect_cpu_time = false
        report_active = false
    [[inputs.disk]]
        ignore_fs = ["tmpfs", "devtmpfs", "devfs"]
    [[inputs.io]]
    [[inputs.mem]]
    [[inputs.net]]
    [[inputs.system]]
    [[inputs.swap]]
    [[inputs.netstat]]
    [[inputs.processes]]
    [[inputs.kernel]]
    
    # Output Plugin InfluxDB
    [[outputs.influxdb]]
      database = "telegraf"
      urls = [ "http://127.0.0.1:8086" ]
      username = "telegraf"
      password = "hakase-ndlr"
```
Save and exit.

Note:

Telegraf provides telegraf command to manage the configuration, including generate the configuration itself, run the command as below.

```
    telegraf config -input-filter cpu:mem:disk:swap:system -output-filter influxdb > telegraf.conf
    cat telegraf.conf
```

Restart the telegraf service and make sure there is no error.
```
    sudo systemctl restart telegraf
```

Now test the telegraf settings using the command below.
```
    sudo telegraf -test -config /etc/telegraf/telegraf.conf --input-filter cpu
    sudo telegraf -test -config /etc/telegraf/telegraf.conf --input-filter net
    sudo telegraf -test -config /etc/telegraf/telegraf.conf --input-filter mem
```
The InfluxDB and Telegraf configuration has been completed.

### Step 6 - Install Grafana

In this step, we will install the beautiful Grafana Dashboard for data visualization.

Add the grafana key and repository.
```
    sudo apt-get install -y apt-transport-https
    sudo apt-get install -y software-properties-common wget
    wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
```

Add this repository for stable releases:
```
    echo "deb https://packages.grafana.com/enterprise/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
```

Add this repository if you want beta releases:
```
    echo "deb https://packages.grafana.com/enterprise/deb beta main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
```
After you add the repository:
```
    sudo apt-get update
    sudo apt-get install grafana-enterprise
    sudo apt-get install grafana
```

https://grafana.com/docs/grafana/latest/installation/debian/
After the installation is complete, start the grafana service and enable it to launch everytime at system boot.
```
    sudo systemctl start grafana-server
    sudo systemctl enable grafana-server
```

The grafana-server is up and running on default port '3000', check it using netstat.
```
    netstat -plntu
```