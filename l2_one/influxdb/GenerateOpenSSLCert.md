## Generating SSL Cert

origin link: https://www.baeldung.com/openssl-self-signed-cert

# 1. Overview

OpenSSL is an open-source command-line tool that allows users to perform various SSL-related tasks.
In this article, we'll learn how to create a self-signed certificate with OpenSSL.

# 2. Creating a Private Key

First, we'll create a private key. 
A private key helps to enable encryption and is the most important component of our certificate.
Let's create a password-protected, 2048-bit RSA private key (domain. key) with the openssl command:

```
    openssl genrsa 0des4 -out domain.key 2048
```

Enter a password when prompted. The output will look like:

```
    Generating RSA private key, 2048 bit long modulus (2 primes)
    .....................+++++
    .........+++++
    e is 65537 (0x010001)
    Enter pass phrase for domain.key:
    Verifying - Enter pass phrase for domain.key:
```
If we want our private key unencrypted, we can simply remove the -des3 option from the command.

# 3. Creating a Certificate Signing Request

IF we want our certificate signed, we need a certificate signing request (CSR).
The CSR includes the public key and some additional information (such as organization and country).

Let's create a CSR (domain.csr) from our existing private key:
```
    openssl req -key domain.key -new -out domain.csr
```
We'll enter our private key password and some CSR information to complete the process. The output will look like:
```
    Enter pass phrase for domain.key:
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:AU
    State or Province Name (full name) [Some-State]:stateA                        
    Locality Name (eg, city) []:cityA
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:companyA
    Organizational Unit Name (eg, section) []:sectionA
    Common Name (e.g. server FQDN or YOUR name) []:domain
    Email Address []:email@email.com
    
    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:
    An optional company name []:

```
An important field is “Common Name”, which should be the exact Fully Qualified Domain Name (FQDN) of our domain.
“A challenge password” and “An optional company name” can be left empty.

We can also create both the private key and CSR with a single command:
```
    openssl req -newkey rsa:2048 -keyout domain.key -out domain.csr
```
If we want our private key unencrypted, we can add the -nodes option:

```
    openssl req -newkey rsa:2048 -nodes -keyout domain.key -out domain.csr
```